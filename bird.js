class Bird {
  constructor(walls, start_pos = undefined, speed = 1) {
    if(!start_pos){
      this.x = width/6;
    } else {
      this.x  = start_pos
    }
    this.y = 0;
    this.size = height / 6;
    this.speed = 0.0;
    this.maxspeed = 25;
    this.eventSpeed = gravity*8;
    this.alive = 1;
    this.score = 0;
    this.findClosestWall();
  }

  kill(){
    this.alive = 0;
    this.killtime = Date.now();
  }

  revive(){
    if(Date.now() - this.killtime > 1000){
      generateWalls() ;
      this.score = 0;
      this.alive = 1;
      this.y = height / 3;
    }
  }

  findClosestWall() {
    var newClosestWallIdx = 0;
    var closestDistance = 1000000;
    for(i=0; i < walls.length; i++){
      var distance = walls[i].x - this.x;
      if(distance > -this.size/2){
        if(distance < closestDistance){
          closestDistance = distance;
          newClosestWallIdx = i;
        }
      }
    }
    this.closestWallIdx = newClosestWallIdx;
  }

  update() {
    this.y -= this.speed;
    if(inputAction){
      this.speed = this.eventSpeed;
    } else{
      if(this.speed > -this.maxspeed){
        this.speed -= gravity;
      }
    }
    if(this.y > height*1.2){
      this.kill()
    }

    var closestWall = walls[this.closestWallIdx];
    var distance =  closestWall.x - this.x;
    if(distance < this.size / 2){
      if(this.y > closestWall.gapHeight + closestWall.gapSize / 2
      || this.y < closestWall.gapHeight - closestWall.gapSize / 2){
        this.kill();
      }
      if(distance < -this.size / 2){
        this.findClosestWall();
        this.score += 1;
      }
    }
    // walls[this.closestWallIdx].gapHeight = this.y
  }

  draw() {
    let angle = atan(this.speed/20, scrollSpeed)
    translate(this.x, this.y)
    rotate(-angle);
    fill(51);
    rect(-this.size/4, -this.size/4, this.size/2, this.size/2, this.size/8);
    fill(255);
    rect(this.size/4, -this.size/4, this.size/4, this.size/4, this.size/8);
  }
}
