function setup() {
  width = 800
  height = 500
  gapSize = height / 3;
  nLines = 3;
  gravity = 2;
  inputAction = 0;
  scrollSpeed = 5;

  createCanvas(width, height);
  stroke(255);
  frameRate(30);
  generateWalls();
  bird = new Bird(walls);
}

function generateWalls() {
  walls = []
  for(i = 0; i<nLines; i++){
    walls.push(new Wall(width + i*width/nLines, speed=scrollSpeed))
  }
}

function draw() {
  push();
  strokeWeight(6);
  if(bird.alive){
    background(120, 0, 100);
    for(i=0; i<walls.length; i++){
      wall = walls[i]
      wall.update()
      wall.draw()
    }
    bird.update()
    bird.draw()
  } else {
    background(200, 0, 0);
    size = height / 8;
    textSize(size);
    text(':( Bird stopped working', width / 8, (this.height + size) / 2);
    if(inputAction){
      bird.revive();
    }
  }
  pop();
  textSize(height / 8);
  text(bird.score, width*9/10, height/8);

}
