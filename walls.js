class Wall {
  constructor(start_pos = undefined, speed = 1) {
    if(!start_pos){
      this.x = width
    } else {
      this.x  = start_pos
    }
    this.gapSize = gapSize;
    this.randomizeGap();
  }

  randomizeGap() {
    this.gapHeight = Math.round(Math.random()*(height-this.gapSize)) + this.gapSize / 2;
  }

  update() {
    this.x -= speed;
    if(this.x < 0){
      this.x = width;
      this.randomizeGap();
    }
  }

  draw() {
    line(this.x, 0, this.x, this.gapHeight - this.gapSize / 2);
    line(this.x, this.gapHeight + this.gapSize / 2, this.x, height);
  }
}
